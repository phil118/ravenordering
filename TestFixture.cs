using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Fabrik.CMS.Domain;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;

namespace RavenOrderingTest
{
    public class Class1
    {
        [Fact]
        public void ControlTest()
        {
            var projectsList = new List<string>() {
                "projects/1634",
                "projects/1637",
                "projects/1639",
                "projects/1635",
                "projects/1640",
                "projects/1641"
            };

            var projectsList2 = new List<string>() {
                "projects/1634",
                "projects/1637",
                "projects/1639",
                "projects/1635",
                "projects/1640",
                "projects/1641"
            };

            Assert.Equal(projectsList, projectsList2);
        }

        [Fact]
        public void FailingTest()
        {   
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var path = AppDomain.CurrentDomain.BaseDirectory;
            X509Certificate2 clientCertificate = new X509Certificate2(path + "../../../free.fabrik.client.certificate.pfx");

            var store = new DocumentStore
            {
                Urls = new[] { "https://a.free.fabrik.ravendb.cloud" },
                Database = "fabrik-dev-5",
                Certificate = clientCertificate
            };

            store.Initialize();

            using (IDocumentSession session = store.OpenSession())
            {
                var siteId = $"sites/129";

                string sortPortfolio = $"{siteId}/portfolio";
                string sortPortfolioKey = sortPortfolio.Replace("/", "-");

                var query = session.Advanced.DocumentQuery<Project>("Projects/Search")
                        .WaitForNonStaleResults()
                        .Statistics(out QueryStatistics stats)
                        .WhereEquals(x => x.SiteId, siteId)
                        // Sorting
                        .AddOrder(sortPortfolioKey, false, OrderingType.Long);

                var orderedList = query.ToList();

                // Load the ordering document for comparision
                var definedOrderPortfolio = session.Load<Portfolio>(sortPortfolio);

                var index = 0;
                var stringIdsList = orderedList.Select(x => x.Id).ToList();

                foreach (var projectId in stringIdsList)
                {
                    var definedOrderProject = definedOrderPortfolio.Projects.ElementAt(index);
                    Console.WriteLine($"Projects from query {projectId} should be project {definedOrderProject} ... index {index}");
                    Console.WriteLine($"is in correct position = {projectId == definedOrderProject}");
                    index++;
                }

                Assert.Equal(definedOrderPortfolio.Projects, stringIdsList);
            }
        }
    }
}